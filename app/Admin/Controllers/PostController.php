<?php

namespace App\Admin\Controllers;

//use App\Model\test;
use App\Model\post;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

use App\Admin\Extensions\ExcelExpoter;	//添加導出

class PostController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('測試標題')
            ->description('測試副標題')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed   $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('奖项')
            ->description('检视')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed   $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('測試編輯標題')
            ->description('測試编辑副標題')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('測試新建標題')
            ->description('測試新建副標題')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new post);

		//禁用创建按钮
		//$grid->disableCreateButton();
		
		// 關閉篩選器
		//$grid->disableFilter();

		// 關閉選擇器
		//$grid->disableRowSelector();
		
		//禁用分页条
		//$grid->disablePagination();
		
		//禁用导出数据按钮
		//$grid->disableExport();
		
		//禁用左側行选择checkbox
		//$grid->disableRowSelector();
		
		//设置底部分页选择器选项
		//$grid->perPages([10, 20, 30, 40, 50]);
		
		// 自訂搜尋
		$grid->filter(function ($filter) {
			// Remove the default id filter
			$filter->disableIdFilter();

			$filter->column(1/2, function ($filter) {

				$filter->equal('a')->url();
				
				$filter->equal('b')->email();

				$filter->equal('c')->integer();

				$filter->equal('d')->ip();

				$filter->equal('e')->mac();
			
			});
			
			$filter->column(1/2, function ($filter) {

			
				$filter->equal('f')->mobile();
				
				$filter->in('gender')->checkbox([
					'm'    => 'Male',
					'f'    => 'Female',
				]);
				
				$filter->equal('released')->radio([
					''   => 'All',
					0    => 'Unreleased',
					1    => 'Released',
				]);
				
				$filter->equal('column')->datetime(['format' => 'YYYY-MM-DD']);
				
				$filter->group('rate', function ($group) {
					
					$group->equal('等于');
					$group->notEqual('不等于');
					$group->gt('大于');
					$group->lt('小于');
					$group->nlt('大于等于');
					$group->ngt('小于等于');
					$group->match('匹配');
					$group->like('like查詢');
					
		});
			
			});
			// 過濾獎項
			$filter->scope('male', '男性')->where('gender', 'm');

			// 多条件查询
			$filter->scope('new', '最近修改')
				->whereDate('created_at', date('Y-m-d'))
				->orWhere('updated_at', date('Y-m-d'));

			// 关联关系查询
			$filter->scope('address')->whereHas('profile', function ($query) {
				$query->whereNotNull('address');
			});

			$filter->scope('trashed', '被软删除的数据')->onlyTrashed();
			

		});
		
		
		$grid->actions(function ($actions) {
			/*
			$actions->disableEdit();	//編輯
			$actions->disableDelete();	//刪除
			$actions->disableView();	//查看
			*/
		}
		
		);
		//$grid->model()->orderBy('place');	//初始排序設定
		//$grid->column('reward_id', '编号');
		//$grid->column('name', '奖项名称');
		//$grid->column('amount', '金额');
		//$grid->column('img', '图片')->image('/admin/upload/', 60, 60); // 路徑請參考 config/filesystems.php, disk => admin => root
		//$grid->column('img_review', '图片 (查询用)')->image('/admin/upload/', 60, 60); // 路徑請參考 config/filesystems.php, disk => admin => root
		//$grid->column('probability', '机率');
		//$grid->column('quantity', '数量');
		//$grid->column('sent_quantity', '已抽中数量');
		
		$grid->exporter(new ExcelExpoter());	//更改導出數據格式(函式位置:app/Admin/Extensions/ExcelExpoter)
		/*一對一關聯*/
		
		/*end*/
		//$grid->column('created_at', '建立日期');
		//$grid->column('updated_at', '更新日期')->sortable();	//排序按鈕

       $grid = new Grid(new Post);

		$grid->id('id')->sortable();
		$grid->title();
		$grid->content();

		$grid->comments('评论数')->display(function ($comments) {
			$count = count($comments);
			return "<span class='label label-warning'>{$count}</span>";
		});

		$grid->created_at();
		$grid->updated_at();

		return $grid;

		$grid = new Grid(new Comment);
		$grid->id('id');
		$grid->post()->title();
		$grid->content();

		$grid->created_at()->sortable();
		$grid->updated_at();

		return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(test::findOrFail($id));

        $show->reward_id('Reward id');
        $show->name('Name');
        $show->amount('Amount');
        $show->img('Img');
        $show->img_review('Img review');
        $show->probability('Probability');
        $show->quantity('Quantity');
        $show->sent_quantity('Sent quantity');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new test);

		// 關閉操作按鈕
		$form->tools(function (Form\Tools $tools) {
			
			//$tools->disableList();	//列表
			//$tools->disableDelete();	//刪除
			$tools->disableView();	//查看
			
		});
		
		//關閉底部按鈕
		$form->footer(function ($footer) {

			// 去掉`重置`按钮
			//$footer->disableReset();

			// 去掉`提交`按钮
			//$footer->disableSubmit();

			// 去掉`查看`checkbox
			$footer->disableViewCheck();

			// 去掉`继续编辑`checkbox
			$footer->disableEditingCheck();

			// 去掉`继续创建`checkbox
			$footer->disableCreatingCheck();

		});
		
		//分隔內容
		$form->tab('分隔內容一', function ($form) {

		$form->text('a')->default('text...')->help('help...');
		$form->text('b')->value('text...');
		$form->text('c')->placeholder('请输入。。。');
		// 设置时间格式，更多格式参考http://momentjs.com/docs/#/displaying/format/
		$form->time('d','e')->format('HH:mm:ss');
		// 设置日期格式，更多格式参考http://momentjs.com/docs/#/displaying/format/
		$form->date('f','g')->format('YYYY-MM-DD');
		// 设置日期格式，更多格式参考http://momentjs.com/docs/#/displaying/format/
		$form->datetime('h','i')->format('YYYY-MM-DD HH:mm:ss');
		

		})->tab('分隔內容二', function ($form) {

		$form->select('j','k')->options([1 => 'foo', 2 => 'bar', 'val' => 'Option name']);
		$form->multipleSelect('l','m')->options([1 => 'foo', 2 => 'bar', 'val' => 'Option name']);
		$form->listbox('n','o')->options([1 => 'foo', 2 => 'bar', 'val' => 'Option name']);
		$form->textarea('p','q')->rows(10);
		$form->checkbox('r','s')->options([1 => 'foo', 2 => 'bar', 'val' => 'Option name']);
		
		})->tab('分隔內容三', function ($form) {

		$form->email('t','u');
		$form->password('v','w');
		$form->url('x','y');
		$form->ip('z','aa');
		$form->mobile('bb','cc')->options(['mask' => '999 9999 9999']);
		$form->color('dd','ee')->default('#ccc');
		
		
		})->tab('分隔內容四', function ($form) {

		$form->timeRange('ff','gg', 'hh');
		$form->dateRange('ii','jj', 'kk');
		$form->datetimeRange('ll','mm', 'nn');
		// 设置单位符号
		$form->currency('oo','pp')->symbol('￥');
		// 设置最大值
		// 设置最小值
		$form->number('qq','rr')->min(10)->max(100);
		$form->rate('ss','tt');
		})->tab('分隔內容五', function ($form) {

		// 添加图片删除按钮
		$form->image('uu','vv')->removable();
		// 并设置上传文件类型
		$form->file('ww','xx')->rules('mimes:doc,docx,xlsx');
		// 多图
		$form->multipleImage('yy','zz');
		// 多文件
		$form->multipleFile('aaa','bbb');
		$form->slider('ccc','ddd')->options(['max' => 100, 'min' => 1, 'step' => 1, 'postfix' => 'years old']);
		
		})->tab('分隔內容六', function ($form) {
		
		//隐藏域
		$form->hidden('eee');
		$form->switch('fff','ggg')->states([
			'on'  => ['value' => 1, 'text' => '是', 'color' => 'primary'],
			'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
			]);
		//只显示字段，不做任何操作
		$form->display('hhh','iii');
		$form->html('你的html内容', $label = '');
		$form->icon('icon');
		
		});

		
        return $form;
    }
}
